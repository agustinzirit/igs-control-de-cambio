import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormArray, FormGroup, FormControl } from '@angular/forms';
import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';

import { Empresa } from '../../../../Class/empresa';
import { EmpresasService } from '../../../../Service/empresas.service';

@Component({
  selector: 'app-registrar-empresas',
  templateUrl: './registrar-empresas.component.html',
  styleUrls: ['./registrar-empresas.component.css']
})
export class RegistrarEmpresasComponent implements OnInit {
  myGroup;
  empresa = new Empresa();
  test;
  constructor(private formBuilder: FormBuilder,
              private empresaService: EmpresasService ) { 

  /*this.myGroup = new FormGroup({
       firstName: new FormControl()
    });*/
 }
  get Nombre_empresa() {
    return this.registerForm.get('Nombre_empresa');
  }

  get Rif_empresa() {
    return this.registerForm.get('Rif_empresa');
  }

  get sucursales(){
    return this.registerForm.get('sucursales') as FormArray;
  }

  

  ngOnInit() {
  }

  registerForm = this.formBuilder.group({
    Nombre_empresa: ['', {
      validators: [Validators.required],
      updateOn: 'blur'
    }],
    Rif_empresa: ['', {
      validators: [Validators.required, Validators.minLength(4)]
    }],
    sucursales: this.formBuilder.array([])
  });

    agregarSucursal(){
	    const SucursalFormGroup  = this.formBuilder.group({
	     Nombre_sucursal : '',
	     Telefono_sucursal: ''
	    });
	    this.sucursales.push(SucursalFormGroup);
	  }

	  removerTelefono(indice: number) {
	    this.sucursales.removeAt(indice);
	  }
	  refrescar() {
    this.registerForm.patchValue({
      Nombre_empresa: '',
      Rif_empresa: '',
    });
    this.sucursales.controls.splice(0, this.sucursales.length);
  }
  mostrar(){
    console.log(this.registerForm.value);
    this.refrescar();
  }

  GuardarEmpresa(): void {
console.log(this.registerForm.value);
        this.empresaService.addEmpresa(this.registerForm.value)
        .subscribe( res => {
            //this.showSuccess();
            console.log("Guardado");
        }, err => {

        /*  if (err.error.details.name === 'SequelizeUniqueConstraintError') {
            console.log("El error es de duplicidad" , err.error.details.name);
           // console.log(err.error.details.detail);
           // this.msgError = "El Servicio ya existe"; 
           // this.showError(this.msgError);
          }else{
            console.log("El error es de campo nulo" , err.error.details.name);
            console.log(err.error.details.errors[0].message);
            //this.msgError = err.error.details.errors[0].message;
            //this.showError(this.msgError);
            console.log("Error");
          }*/console.log(err);
        });
        // this.mostrar();
  }

  }

