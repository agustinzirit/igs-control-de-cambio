import { Component } from '@angular/core';
import { Cargo } from '../../../Class/cargo';
import { CargosService } from "../../../Service/cargos.service";

import { Location } from '@angular/common';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-registrar-cargos',
  templateUrl: './registrar-cargos.component.html',
  styleUrls: ['./registrar-cargos.component.css']
})
export class RegistrarCargosComponent   {

  cargo = new Cargo();
  submitted = false;
  msgError;

  constructor(private cargosService: CargosService,
              private location: Location,
              private toastr: ToastrService
              ){}

  showSuccess() {
    this.toastr.success('Registro exitoso', 'Servicio Incluido!', {
      progressBar: true
    });
  }
  showError(msgError: string) {
    this.toastr.error(msgError ,'Error en el registro',{
      progressBar: true
    });

  }
  newCargo(): void {
    this.submitted = false; 
    this.cargo = new Cargo();
  }
  addCargo() {
    this.submitted = true; 

    if (this.cargo.Nombre_cargo != undefined) {
     console.log(this.cargo.Nombre_cargo);
     const resto = this.cargo.Nombre_cargo.toLowerCase();
     this.cargo.Nombre_cargo = this.cargo.Nombre_cargo.charAt(0).toUpperCase() + resto.slice(1);
   // this.servicio.Nombre_servicio = this.servicio.Nombre_servicio.toLowerCase();
    }
    this.save(); 
  }
  goBack(): void {
    this.location.back();
  }
  private save(): void {
    this.cargosService.addCargo(this.cargo)
        .subscribe( res => {
            this.showSuccess();
        }, err => {

          if (err.error.details.name === 'SequelizeUniqueConstraintError') {
            console.log("El error es de duplicidad" , err.error.details.name);
           // console.log(err.error.details.detail);
            this.msgError = "El Cargo ya existe"; 
            this.showError(this.msgError);
            console.log(this.cargo.Nombre_cargo);
          }else{
            console.log("El error es de campo nulo" , err.error.details.name);
            console.log(err.error.details.errors[0].message);
            this.msgError = err.error.details.errors[0].message;
            this.showError(this.msgError);
          }
        });
  }
}
