import { Component, OnInit } from '@angular/core';
import { Cargo } from '../../../Class/cargo';
import { CargosService } from '../../../Service/cargos.service';

import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-datos-cargo',
  templateUrl: './datos-cargo.component.html',
  styleUrls: ['./datos-cargo.component.css']
})
export class DatosCargoComponent implements OnInit {

  cargo = new Cargo();
  submitted = false; 
  message: string;;
  msgError;
  constructor(private cargosService: CargosService,
              private route: ActivatedRoute,
              private location: Location,
              private toastr: ToastrService) {}

  
  showSuccess() {
	this.toastr.success('Actualizacion realizada', 'Cargo actualizado!', {
	progressBar: true
	});
  }
   showError(msgError: string) {
    this.toastr.error(msgError ,'Error en el registro',{
      progressBar: true
    });

  }
  ngOnInit(): void {
  	const id = +this.route.snapshot.paramMap.get("id");
  	console.log(id)
  	this.cargosService.getCargo(id)
  		.subscribe(cargo => {this.cargo = cargo; console.log(id)});
  }
  updateCargo(){
       this.submitted = true; 
       if(this.cargo.Nombre_cargo.length > 0 && this.cargo.Nombre_cargo.length != undefined) {
       console.log("Nombre del CArgo !!! OJO ",this.cargo.Nombre_cargo.length);
       console.log(typeof(this.cargo.Nombre_cargo.length))
       console.log(this.cargo.Nombre_cargo.length)
       const resto = this.cargo.Nombre_cargo.toLowerCase();
       this.cargo.Nombre_cargo = this.cargo.Nombre_cargo.charAt(0).toUpperCase() + resto.slice(1);
       // this.servicio.Nombre_servicio = this.servicio.Nombre_servicio.toLowerCase();
       this.update();
      }else{
            this.msgError = "El nombre no puede estar vacio"; 
            this.showError(this.msgError);
      }
  }
  private update(): void {
	    this.submitted = true;
	    this.cargosService.updateCargo(this.cargo)
	        .subscribe(res => {
            this.showSuccess();
            console.log("Este es el nombre del cargo",this.cargo.Nombre_cargo);
          }, err => {
            if (err.error.details.name === 'SequelizeUniqueConstraintError') {
            console.log("El error es de duplicidad" , err.error.details.name);
           // console.log(err.error.details.detail);
            this.msgError = "El Cargo ya existe"; 
            this.showError(this.msgError);
          }else if(err.error.details.name ==="SequelizeValidationError"){
              console.log("El error es de campo nulo" , err.error.details.name);
              console.log(err.error.details.errors[0].message);
              this.msgError = err.error.details.errors[0].message;
              this.showError(this.msgError);
            }
        });
  }
  delete(): void {
  	this.submitted = true; 
  	this.cargosService.deleteCargo(this.cargo.Id_tipo_cargo)
  		.subscribe(() => this.message = "Cargo eliminado correctamente");

  }




}
