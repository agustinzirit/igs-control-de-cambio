import { Component, OnInit } from '@angular/core';

import { Cargo } from '../../../Class/cargo';
import { CargosService } from '../../../Service/cargos.service';

import { MatTableDataSource, MatSort  } from '@angular/material';
import { TooltipPosition } from '@angular/material/tooltip';
import { FormControl} from '@angular/forms';
import { ActivatedRoute, Params } from "@angular/router";

export interface Tickets {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-listar-cargos',
  templateUrl: './listar-cargos.component.html',
  styleUrls: ['./listar-cargos.component.css']
})

export class ListarCargosComponent implements OnInit {
 

	cargos: Cargo[];
	displayedColumns: string[] = ['Id_tipo_cargo', 'Nombre_cargo', 'Descripcion_cargo', 'Estado_cargo', 'Opciones'];
	foods: Tickets[] = [
	      {value: '0', viewValue: 'Todos'},
	      {value: '1', viewValue: 'Habilitados'},
	      {value: '2', viewValue: 'Deshabilitados'}
	    ];
  constructor(private cargosService: CargosService) { }

 ngOnInit(): void {
  	this.getCargo();
  }
  getCargo(){
  	return this.cargosService.getCargos()
  		.subscribe(
  		    cargos => {
  		    	console.log(cargos);
  		    	this.cargos = cargos;
  		   	}
  		 );
  }
}
