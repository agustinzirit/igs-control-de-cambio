import { Component, OnInit} from '@angular/core';

import { Servicio } from '../../../Class/servicio';
import { ServiciosService } from '../../../Service/servicios.service';

import { MatTableDataSource, MatSort  } from '@angular/material';
import { TooltipPosition } from '@angular/material/tooltip';
import { FormControl} from '@angular/forms';
import { ActivatedRoute, Params } from "@angular/router";


export interface Tickets {
  value: string;
  viewValue: string;
}


@Component({
  selector: 'app-listar-servicios',
  templateUrl: './listar-servicios.component.html',
  styleUrls: ['./listar-servicios.component.css']
})

export class ListarServiciosComponent implements OnInit {

	servicios: Servicio[];
  displayedColumns: string[] = ['Id_servicio_app', 'Nombre_servicio', 'Descripcion_servicio', 'Estado_servicio', 'Opciones'];
   foods: Tickets[] = [
      {value: '0', viewValue: 'Todos'},
      {value: '1', viewValue: 'Habilitados'},
      {value: '2', viewValue: 'Deshabilitados'}
    ];
  constructor(private servicioService: ServiciosService) {}

  ngOnInit(): void {
  	this.getServicios(); 
   
  }

  getServicios() {
  	return this.servicioService.getServicios()
  		.subscribe(
  		    servicios => {
  		    	console.log(servicios);
  		    	this.servicios = servicios;
  		   	}
  		 );
  }

}
