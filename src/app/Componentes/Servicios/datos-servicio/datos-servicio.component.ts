import { Component, OnInit } from '@angular/core';
import { Servicio } from '../../../Class/servicio';
import { ServiciosService } from '../../../Service/servicios.service';

import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-datos-servicio',
  templateUrl: './datos-servicio.component.html',
  styleUrls: ['./datos-servicio.component.css']
})
export class DatosServicioComponent implements OnInit {

	servicio = new Servicio();
	submitted = false; 
	message: string;;
  msgError;

  constructor(
              private serviciosService: ServiciosService,
              private route: ActivatedRoute,
              private location: Location,
              private toastr: ToastrService
              ) {}
  ////////////////////////////////////////////////////
  showSuccess() {
    this.toastr.success('Actualizacion realizada', 'Servicio actualizado!', {
     progressBar: true
    });
  }




  ///////////////////////////////////////////////////////////////////
  showError(msgError: string) {
    this.toastr.error(msgError ,'Error en el registro',{
      progressBar: true
    });

  }



  /////////////////////////////////////////////////////
  ngOnInit(): void {
  	const id = +this.route.snapshot.paramMap.get("id");
  	console.log(id)
  	this.serviciosService.getServicio(id)
  		.subscribe(servicio => {this.servicio = servicio; console.log(id)});
  }



  updateService(){
      this.submitted = true; 
       if(this.servicio.Nombre_servicio.length > 0 && this.servicio.Nombre_servicio != undefined) {
       console.log("Nombre del Servicio !!! OJO ",this.servicio.Nombre_servicio);
       console.log(typeof(this.servicio.Nombre_servicio))
       console.log(this.servicio.Nombre_servicio.length)
       const resto = this.servicio.Nombre_servicio.toLowerCase();
       this.servicio.Nombre_servicio = this.servicio.Nombre_servicio.charAt(0).toUpperCase() + resto.slice(1);
       // this.servicio.Nombre_servicio = this.servicio.Nombre_servicio.toLowerCase();
       this.update();
      }else{
            this.msgError = "El nombre no puede estar vacio"; 
            this.showError(this.msgError);
      }
     }
//////////////////////////////////////////////////////
  private update(): void {
	    this.submitted = true;
	    this.serviciosService.updateServicio(this.servicio)
	        .subscribe(res => {
            this.showSuccess();
            console.log("Este es el nombre del servicio",this.servicio.Nombre_servicio);
          }, err => {
            if (err.error.details.name === 'SequelizeUniqueConstraintError') {
            console.log("El error es de duplicidad" , err.error.details.name);
           // console.log(err.error.details.detail);
            this.msgError = "El Servicio ya existe"; 
            this.showError(this.msgError);
          }else if(err.error.details.name ==="SequelizeValidationError"){
              console.log("El error es de campo nulo" , err.error.details.name);
              console.log(err.error.details.errors[0].message);
              this.msgError = err.error.details.errors[0].message;
              this.showError(this.msgError);
            }
        });
  }
//////////////////////////////////////////////////////////////
  delete(): void {
  	this.submitted = true; 
  	this.serviciosService.deleteServicio(this.servicio.Id_servicio_app)
  		.subscribe(() => this.message = "Servicio eliminado correctamente");

  }


  goBack(): void {
  	this.location.back();
  }
}
