import { Component } from '@angular/core';
import { Servicio } from '../../../Class/servicio';
import { ServiciosService } from '../../../Service/servicios.service';

import { Location } from '@angular/common';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-registrar-servicios',
  templateUrl: './registrar-servicios.component.html',
  styleUrls: ['./registrar-servicios.component.css']
})
export class RegistrarServiciosComponent  {

  servicio = new Servicio();
  submitted = false;
  msgError;

  
  constructor(private servicioService: ServiciosService,
              private location: Location,
              private toastr: ToastrService
              ){}

  showSuccess() {
    this.toastr.success('Registro exitoso', 'Servicio Incluido!', {
      progressBar: true
    });
  }
  showError(msgError: string) {
    this.toastr.error(msgError ,'Error en el registro',{
      progressBar: true
    });

  }
//////////////////////////////////////////////////////////
  newServicio(): void {
    this.submitted = false; 
    this.servicio = new Servicio();

  }
//////////////////////////////////////////////////
  addService() {
    this.submitted = true; 

    if (this.servicio.Nombre_servicio != undefined) {
     console.log(this.servicio.Nombre_servicio);
     const resto = this.servicio.Nombre_servicio.toLowerCase();
     this.servicio.Nombre_servicio = this.servicio.Nombre_servicio.charAt(0).toUpperCase() + resto.slice(1);
   // this.servicio.Nombre_servicio = this.servicio.Nombre_servicio.toLowerCase();
    }
    
    this.save(); 
  }
///////////////////////////////////////////////////////
  goBack(): void {
    this.location.back();
  }
////////////////////////////////////////////////////////
  private save(): void {
    this.servicioService.addServicio(this.servicio)
        .subscribe( res => {
            this.showSuccess();
        }, err => {

          if (err.error.details.name === 'SequelizeUniqueConstraintError') {
            console.log("El error es de duplicidad" , err.error.details.name);
           // console.log(err.error.details.detail);
            this.msgError = "El Servicio ya existe"; 
            this.showError(this.msgError);
          }else{
            console.log("El error es de campo nulo" , err.error.details.name);
            console.log(err.error.details.errors[0].message);
            this.msgError = err.error.details.errors[0].message;
            this.showError(this.msgError);
          }
        });
  }
}

