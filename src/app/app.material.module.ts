import { NgModule } from '@angular/core';

import { 
	MatButtonModule ,
	MatSidenavModule,
	MatGridListModule ,
	MatMenuModule,
	MatIconModule,
	MatCardModule,
	MatToolbarModule,
	MatListModule,
	MatFormFieldModule,
	MatSelectModule,
	MatRadioModule,
	MatInputModule,
	MatCheckboxModule,
	MatTabsModule,
	MatTableModule,
	MatPaginatorModule,
	MatTooltipModule
	
} from '@angular/material/';

@NgModule({
	imports: [ 
	MatButtonModule, 
	MatSidenavModule,
	MatGridListModule,
	MatMenuModule,
	MatIconModule,
	MatCardModule,
	MatToolbarModule,
	MatListModule,
	MatFormFieldModule,
	MatSelectModule,
	MatRadioModule,
	MatInputModule,
	MatCheckboxModule,
	MatTabsModule,
	MatTableModule,
	MatPaginatorModule,
	MatTooltipModule 
	],
	exports: [ 
	MatButtonModule,
	 MatSidenavModule,
	 MatGridListModule,
	 MatMenuModule,
	 MatIconModule,
	 MatCardModule,
	 MatToolbarModule,
	 MatListModule,
	 MatFormFieldModule,
	 MatSelectModule,
	 MatRadioModule,
	 MatInputModule,
	 MatCheckboxModule,
	 MatTabsModule,
	 MatTableModule,
	 MatPaginatorModule,
	 MatTooltipModule 
	]
})
export class MaterialModule{}