import { NgModule } from '@angular/core';
import { RouterModule,  Route } from '@angular/router';


//import { AppComponent } from './app.component';
import { ListaComponent } from '../Componentes/lista/lista.component';
import { UsuarioComponent } from '../Componentes/usuario/usuario.component';
import { AboutComponent} from '../Componentes/about/about.component';
import { DashboardComponent } from '../Componentes/dashboard/dashboard.component';
import { MainNavComponent } from '../Componentes/main-nav/main-nav.component';
import { RegisterUserComponent} from '../Componentes/register-user/register-user.component';
import { RegistroUsuariosComponent } from '../Componentes/registro-usuarios/registro-usuarios.component';
//Servicios
import { RegistrarServiciosComponent } from '../Componentes/Servicios/registrar-servicios/registrar-servicios.component';
import { ListarServiciosComponent } from '../Componentes/Servicios/listar-servicios/listar-servicios.component';
import { DatosServicioComponent } from "../Componentes/Servicios/datos-servicio/datos-servicio.component"
//Cargos
import { RegistrarCargosComponent } from "../Componentes/Cargos/registrar-cargos/registrar-cargos.component";
import { DatosCargoComponent } from "../Componentes/Cargos/datos-cargo/datos-cargo.component";
import { ListarCargosComponent} from "../Componentes/Cargos/listar-cargos/listar-cargos.component";

//Clientes
//Empresa
import { DatosEmpresaComponent } from "../Componentes/Clientes/Empresa/datos-empresa/datos-empresa.component";
import { ListarEmpresasComponent } from "../Componentes/Clientes/Empresa/listar-empresas/listar-empresas.component";
import { RegistrarEmpresasComponent } from "../Componentes/Clientes/Empresa/registrar-empresas/registrar-empresas.component";
//Sucursal

const routes: Route[] = [
  {path: 'Home' , 
  component: MainNavComponent,
  children: [
    {path: 'About', component: AboutComponent},
    {path: 'Resumen', component: DashboardComponent },
    {path: 'RegistrarUsuario', component: RegistroUsuariosComponent },
    {path: 'RegistrarServicio', component: RegistrarServiciosComponent},
    {path: 'ListarServicios', component: ListarServiciosComponent},
    {path: 'DatosServicios/:id', component: DatosServicioComponent},
    {path: "RegistrarCargos",  component: RegistrarCargosComponent},
    {path: "ListarCargos",  component: ListarCargosComponent},
    {path: "DatosCargo/:id",  component: DatosCargoComponent},
    {path: "RegistrarEmpresa", component: RegistrarEmpresasComponent},
    {path: "ListarEmpresas", component: ListarEmpresasComponent},
    {path: "DatosEmpresa/:id", component: DatosEmpresaComponent}
  ],
  },
  {
    path: '' , 
    component: RegisterUserComponent,
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
 
export class AppRoutingModule {}