import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
 
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { RouterModule, Routes, Route } from '@angular/router';


import { AppComponent } from './app.component';
import { ListaComponent } from './Componentes/lista/lista.component';
import { UsuarioComponent } from './Componentes/usuario/usuario.component';
import {AboutComponent} from './Componentes/about/about.component';
import { DashboardComponent } from './Componentes/dashboard/dashboard.component';
import { MainNavComponent } from './Componentes/main-nav/main-nav.component';
import {RegisterUserComponent} from './Componentes/register-user/register-user.component';
import { RegistroUsuariosComponent } from './Componentes/registro-usuarios/registro-usuarios.component';
//Servicios
import { RegistrarServiciosComponent } from './Componentes/Servicios/registrar-servicios/registrar-servicios.component'; 
import { ListarServiciosComponent} from './Componentes/Servicios/listar-servicios/listar-servicios.component'; 
import { DatosServicioComponent } from "./Componentes/Servicios/datos-servicio/datos-servicio.component"
//Cargos
import { RegistrarCargosComponent } from "./Componentes/Cargos/registrar-cargos/registrar-cargos.component";
import { DatosCargoComponent } from "./Componentes/Cargos/datos-cargo/datos-cargo.component";
import { ListarCargosComponent} from "./Componentes/Cargos/listar-cargos/listar-cargos.component";
//Clientes
//Empresa
import { DatosEmpresaComponent } from "./Componentes/Clientes/Empresa/datos-empresa/datos-empresa.component";
import { ListarEmpresasComponent } from "./Componentes/Clientes/Empresa/listar-empresas/listar-empresas.component";
import { RegistrarEmpresasComponent } from "./Componentes/Clientes/Empresa/registrar-empresas/registrar-empresas.component";
//Sucursal


import { DataService } from './Service/data.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MaterialModule } from './app.material.module';

//Rutas
import { AppRoutingModule } from './app-routing/app-routing.module';



import { ToastrModule } from 'ngx-toastr';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';

@NgModule({
  declarations: [
    AppComponent,
    ListaComponent,
    UsuarioComponent,
    AboutComponent,
    DashboardComponent,
    MainNavComponent,
    RegisterUserComponent,
    RegistroUsuariosComponent,
    RegistrarServiciosComponent,
    ListarServiciosComponent,
    DatosServicioComponent,
    RegistrarCargosComponent,
    DatosCargoComponent,
    ListarCargosComponent,
    DatosEmpresaComponent,
    ListarEmpresasComponent,
    RegistrarEmpresasComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    ScrollToModule.forRoot(),
    ToastrModule.forRoot() ,
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
