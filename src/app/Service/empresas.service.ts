import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Empresa } from '../Class/empresa';


const httpOptions = {
	headers: new HttpHeaders({'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class EmpresasService {
 
 private empresassUrl = 'http://localhost:8081/api/empresas';
  constructor(
    private http: HttpClient
    ){ }


  addEmpresa (empresa): Observable<Empresa> { 
  	return this.http.post<Empresa>(this.empresassUrl, empresa, httpOptions);
  }
  
}
